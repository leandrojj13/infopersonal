package com.ljj.Dao;

import com.ljj.Entities.Idiomas;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Repository

public class IdiomasDao {


    public List<Idiomas> getIdiomas(){
        Connection con = getConnection();
        List<Idiomas> Idiomas = new ArrayList<Idiomas>() {
        };
        if(con == null){
            return null;
        }else {
            Statement state;
            String query = "Select * from Idiomas where Deleted = 0";
            try{
                state = con.createStatement();
                ResultSet rs = state.executeQuery(query);

                while(rs.next()){
                    Idiomas newIdiomas = new Idiomas(
                            rs.getInt("Id"),
                            rs.getString("Nombre"),
                            rs.getString("Descripcion"),
                            rs.getBoolean("Deleted"),
                            rs.getDate("CreatedDate"),
                            rs.getDate("ModifiedDate")
                    );
                    newIdiomas.setId(rs.getInt("Id"));
                    Idiomas.add(newIdiomas);
                }
            }catch (SQLException ex){
                return null;
            } catch (Exception e){
                return null;
            }
            return Idiomas;
        }
    }

    public boolean createIdioma(Idiomas Idioma) {
        Idiomas IdiomaToCheck;
        IdiomaToCheck = getIdiomaByName(Idioma.getNombre());
        if(IdiomaToCheck != null){
            return false;
        }else{
            String query = "INSERT INTO Idiomas VALUES ('"+Idioma.getNombre()+"','"+
                    Idioma.getDescripcion()+"',"+
                    0+",'"+
                    LocalDateTime.now()+"','"+
                    LocalDateTime.now()+"')";
            return executeUpdate(query);

        }
    }

    public boolean deleteIdioma(int id){
        String query = "UPDATE Idiomas SET Deleted = 1 WHERE id ="+ id;
        return executeUpdate(query);
    }

    public Idiomas getIdiomaById(int id){
        String query = "Select * from Idiomas WHERE id ="+ id;
        return getIdioma(query);
    }

    private Idiomas getIdiomaByName(String name){
        String query = "Select * from Idiomas WHERE Nombre ="+ name;
        return getIdioma(query);
    }

    private Idiomas getIdioma(String query){
        try {
            Idiomas Idiomas = new Idiomas();
            Connection con = getConnection();
            Statement state;
            state = con.createStatement();
            ResultSet rs = state.executeQuery(query);

            while (rs.next()) {
                Idiomas newIdiomas = new Idiomas(
                        rs.getInt("Id"),
                        rs.getString("Nombre"),
                        rs.getString("Descripcion"),
                        rs.getBoolean("Deleted"),
                        rs.getDate("CreatedDate"),
                        rs.getDate("ModifiedDate")
                );
                newIdiomas.setId(rs.getInt("Id"));
                Idiomas = newIdiomas;
            }
            return Idiomas;
        } catch (SQLException ex) {
            return null;
        } catch (Exception e) {
            return null;
        }
    }
    private boolean executeUpdate(String query){
        try {
            Connection con = getConnection();
            Statement state;
            state = con.createStatement();
            int result = state.executeUpdate(query);

            if(result ==1){
                return true;
            }
            return false;
        } catch (SQLException ex) {
            return false;
        } catch (Exception e) {
            return false;
        }
    }

    private Connection getConnection() {
        try{
            String connectionUrl = "jdbc:sqlserver://localhost\\.:1433;database=InfoPersonal;user=admin;password=Admin123,";
            Connection cnn = DriverManager.getConnection(connectionUrl);

            return cnn;
        }catch (SQLException ex){
            return null;
        }

    }

    public boolean updateIdioma(Idiomas idiomas){

            String query = "UPDATE Idiomas SET Nombre = '"+idiomas.getNombre()+
                    "', Descripcion = '"+ idiomas.getDescripcion()+
                    "', ModifiedDate ='"+ LocalDateTime.now() +"' WHERE id ="+ idiomas.getId();
            return executeUpdate(query);


    }


}
