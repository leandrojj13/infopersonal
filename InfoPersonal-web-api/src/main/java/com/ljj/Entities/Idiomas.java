package com.ljj.Entities;

import java.util.Date;

public class Idiomas {

    private int Id;
    private String Nombre;
    private String Descripcion;
    private Boolean Deleted;
    private Date CreatedDate;
    private Date ModifiedDate;

    public Idiomas() {
    }

    public Idiomas(int id, String nombre, String descripcion, Boolean deleted, Date createdDate, Date modifiedDate) {
        Id = id;
        Nombre = nombre;
        Descripcion = descripcion;
        Deleted = deleted;
        CreatedDate = createdDate;
        ModifiedDate = modifiedDate;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public Boolean getDeleted() {
        return Deleted;
    }

    public void setDeleted(Boolean deleted) {
        Deleted = deleted;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date createdDate) {
        CreatedDate = createdDate;
    }

    public Date getModifiedDate() {
        return ModifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        ModifiedDate = modifiedDate;
    }
}

