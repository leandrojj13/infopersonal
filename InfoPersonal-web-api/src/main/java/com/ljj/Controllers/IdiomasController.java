package com.ljj.Controllers;

import com.ljj.Entities.Idiomas;
import com.ljj.Entities.Institutions;
import com.ljj.Services.IdiomasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;
import java.util.List;

@RestController
@RequestMapping("api/Idiomas")
public class IdiomasController {

    @Autowired
    private IdiomasService idiomasService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Idiomas> getAllIdioma(){
        return idiomasService.getAllIdiomas();
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean createIdioma(@RequestBody Idiomas idioma){
        return idiomasService.createIdioma(idioma);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Idiomas getIdiomaById(@PathVariable("id") int id){
        return idiomasService.getIdiomaById(id);
    }

    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public boolean updateIdioma(@RequestBody Idiomas idioma){
        return idiomasService.updateIdioma(idioma);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public boolean deleteIdioma(@PathVariable("id") int id){
        return idiomasService.deleteIdioma(id);
    }
}
