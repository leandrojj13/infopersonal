package com.ljj.Services;

import com.ljj.Dao.IdiomasDao;
import com.ljj.Entities.Idiomas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class IdiomasService {

    @Autowired
    private IdiomasDao idiomasDao;

    public List<Idiomas> getAllIdiomas(){
        return this.idiomasDao.getIdiomas();
    }

    public boolean createIdioma(Idiomas idioma){
        return this.idiomasDao.createIdioma(idioma);
    }
    public boolean deleteIdioma(int id){
        return this.idiomasDao.deleteIdioma(id);
    }

    public boolean updateIdioma(Idiomas idiomas){
        return this.idiomasDao.updateIdioma(idiomas);
    }

    public Idiomas getIdiomaById(int id){
        return this.idiomasDao.getIdiomaById(id);
    }
}
