package com.ljj.Services;

import com.ljj.Dao.InstitutionsDao;
import com.ljj.Entities.Institutions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class InstitutionsService {

    @Autowired
    private InstitutionsDao institutionsDao;


    public List<Institutions> getAllInstitutions(){
        return this.institutionsDao.getInstitutions();
    }
}
