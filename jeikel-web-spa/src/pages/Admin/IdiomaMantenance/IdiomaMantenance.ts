import { GenericPage, Component, Vue } from '../baseExporter'
import { IdiomaModel } from '../../../Models/IdiomaModel'
import axios from 'axios'

@Component({
  template: require('./IdiomaMantenance.html'),
  components: {  }
})
export class IdiomaMantenance extends GenericPage<IdiomaModel> {

  idiomas : Array<IdiomaModel> = [];
  constructor() {
    super('', {
      nombre: {
        label: 'Nombre',
        sortable: true
      },
      descripcion: {
        label: 'Descripcion',
        sortable: false
      },
    }, IdiomaModel)
  }

  mounted() {
      this.getData();
  }

    getData(){
      axios.get("").then((response)=>{
        this.idiomas = response.data
        }).catch(()=>{
          alert("Error")
        })
    }

    saveData(){
        if(this.model.id){
          axios.put("",this.model).then(()=>{

            alert("Editado Correctamente")
          }).catch(()=>{
            alert("Error")
          })

        }else{

          axios.post("",this.model).then((response)=>{
            alert('Guardaddo Correctamente');
            this.getData();
          }).catch(()=>{
            alert("Error")
          })
        }

    }

    mapIdioma(idioma: IdiomaModel){
      this.model =  idioma;

    }

    deleteData(idioma){
      
      var result = confirm("Esta seguro que desea borrar " + idioma.nombre )
      if(result) {
        axios.delete("").then(()=>{
            alert(" Borrado Correctamente")

        }).catch(()=>{
          alert("Error")
        })

      }
        

    }
}
