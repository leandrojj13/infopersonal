import { BaseModel } from './Generic/BaseModel'
export class IdiomaModel extends BaseModel {
  id:number;
  nombre:string;
  descripcion:number;
  constructor() {

    super()
    this.id = null;
    this.nombre = null;
    this.descripcion = null;

  }

}
